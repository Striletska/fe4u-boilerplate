
type: 'module';

import { additionalUsers, randomUserMock } from './FE4U-Lab2-mock.js';
export let editArr = [];
export let currentArray = [];

let favoritesTeachers =[];
let currentPage = "1";
export let teachers = [];
fillArray();
async function fillArray() {
   fetch("https://randomuser.me/api/?results=50")
        .then(response => response.json())
        .then(data => {
         let user = data.results;
         teachers = [...user];
          createArrayTeachers();
          createTeachers(teachers);
          renderFavorites();
          pageInTable();
          drawTable(1, teachers);
          console.log(teachers[0])
        })
        .catch(error => console.log("Warning : " + error))
}
//RENDER A TEACHER SECTION
function randomId() {
  const chars = [1, 2, 3, 4, 5, 6, 7, 8];
  let randomID = '';
  for (let i = 0; i < 8; i++) {
    const randomIndex = Math.floor(Math.random() * chars.length);
    randomID += randomIndex;
  }
  return randomID;
}
function createTeachers(array) {
  const section = document.getElementById('teachers_section');
  let html = '';
  for (let i = 0; i < array.length; i++) {
    if(array[i].favorite) html += `<div class ="accountTeacher favorite" id="${array[i].first_name} ${array[i].last_name}">`

    else  html += `<div class ="accountTeacher" id="${array[i].first_name} ${array[i].last_name}">`
    if (array[i].picture_large === undefined) {
      html
          += `  <div class="cycle-photo without-photo" >
                  <span class="styled-image">${array[i].first_name.charAt(0)}.${array[i].last_name.charAt(0)}</span>
                  </div>`
    } else {
      html
          += ` <div class="cycle-photo" >
                 <img class ="imgClass" src=${array[i].picture_large} alt="Photo is absent"/>
                 </div>`
    }

    html
        += `
             <p class ="name-teacher">${array[i].first_name}</p>
             <p class ="name-teacher last">${array[i].last_name}</p>
             <p class="subject">${array[i].course}</p>
             <p class ="country">${array[i].country}</p> 
             </div>`;
  }
  section.innerHTML = html;
}

// SEARCH BY NOTE, AGE, NAME
document.getElementById("search").addEventListener("click", searchOnPage);
function searchOnPage(){
  let input = document.getElementById("inputSearch").value
  if(input !== ""){
    if(search(input).full_name === undefined) alert("Nothing was found")
    else addInfoPopup(search(input));
  }
}

// FILTER
document.getElementById("age").addEventListener("change",drawFilter);
document.getElementById("region").addEventListener("change",drawFilter);
document.getElementById("sex").addEventListener("change",drawFilter);
document.getElementById("onlyPhoto").addEventListener("change",drawFilter);
document.getElementById("onlyFav").addEventListener("change",drawFilter);

function drawFilter(){
  let filter ={
    country: document.getElementById("region").value,
    age: document.getElementById("age").value,
    gender: document.getElementById("sex").value,
    photo: document.getElementById("onlyPhoto").checked,
    favorite: document.getElementById("onlyFav").checked,
  }
  let teachers = filtration(filter);
  createTeachers(teachers);
}

//ADD INFO POPUP
document.getElementById("close_info_popup").addEventListener("click", function (event) {
  document.getElementById("teacher-info-popup").style.display = 'none';
});

for (const button of document.getElementsByClassName("addTeacherB")) {
  button.addEventListener("click", addTeacherPopup);
}
let teachersSection = document.getElementById("teachers_section");
teachersSection.addEventListener("click", function (event) {
  let teacher = event.target.closest(".accountTeacher");
  addInfoPopup(search(teacher.id));
})
function addInfoPopup(searched_teacher){
  const section = document.getElementById('teacher-info-article');
  const footer_section = document.getElementById('footer_popup');
  let html = ` 
   <img style="float: left" src=${searched_teacher.picture_large} alt="Photo is absent">
   <section class="info">
    <p class="name_teacher_info">${searched_teacher.full_name}</p>
    <p class="p_info">${searched_teacher.course}</p><br>
    <p>${searched_teacher.city}, ${searched_teacher.country}</p><br>
    <p>${searched_teacher.age}, ${searched_teacher.gender}</p><br>
    <p class="p_info2">${searched_teacher.email}</p><br>
    <p>${searched_teacher.phone}</p><br>
    </section> 
    <button class ="addToFav" id ="addToFav"></button>`
  // change star if favorite
  section.innerHTML = html;
  let addToFav = document.getElementById("addToFav");
  if(searched_teacher.favorite) addToFav.style.backgroundImage ="url(\"/src/images/gold-star.png\")";
  else {
    addToFav.style.backgroundImage = "url(\"/src/images/emptyStar.png\")";
  }

  let footerHtml = ` 
      <p style="margin-bottom: 2%">${searched_teacher.note}</p>
      <a href="url">toggle map</a>`
  footer_section.innerHTML = footerHtml;
  let teacherSection = document.getElementById(`${searched_teacher.full_name}`)
  document.getElementById("teacher-info-popup").style.display = 'grid';
  const addToFavButton = section.querySelector('.addToFav');
  addToFavButton.addEventListener('click', function () {
    if(!searched_teacher.favorite){
      teacherSection.className = "accountTeacher favorite";
      addToFavButton.style.backgroundImage = "url(\"/src/images/gold-star.png\")";
      searched_teacher.favorite = true;
      favoritesTeachers.push(searched_teacher);
    }
    else{
      teacherSection.className = "accountTeacher";
      addToFavButton.style.backgroundImage = "url(\"/src/images/emptyStar.png\")";
      searched_teacher.favorite = false;
      favoritesTeachers = favoritesTeachers.filter(function(item) {
        return item !== searched_teacher;
      });
    }
    renderFavorites();
  });

}

// ADD TEACHER POPUP
document.getElementById("cancel-button").addEventListener("click", function (event) {
  document.getElementById("add-teacher-popup").style.display = 'none';
})
function addTeacherPopup(){
  document.getElementById("add-teacher-popup").style.display = "flex";
}
document.forms[0].addEventListener("submit", async (e) => {
  e.preventDefault();
  let inputs = document.forms[0].elements;
  let date = inputs.date.value.split("-");
  let name = inputs.enterName.value.split(" ");
  let gander = "";
  let title = "";
  if (document.getElementById("female").checked) {
    gander = "female";
    title = "Mrs";
  } else if (document.getElementById("male").checked
  ) {
    gander = "male";
    title = "Mr";
  }
  let newTeacher = {
    gender: gander,
    title: title,
    full_name: inputs.enterName.value,
    first_name: name[0],
    last_name: name[1],
    city: inputs.city.value,
    country: inputs.country.value,
    email: inputs.email.value,
    b_date: inputs.date.value,
    age: new Date().getFullYear() - date[0],
    phone: inputs.phone.value,
    favorite: false,
    course: inputs.speciality.value,
    bg_color: inputs.color.value,
    note: inputs.notes.value,
    picture_large: undefined,

  };
  if (!validateObject(newTeacher)) newTeacher = toValidateObj(newTeacher);
  teachers.push(newTeacher);
  createTeachers(teachers);
  currentArray.push(newTeacher);
  drawTable(currentPage, currentArray);
  const response = await fetch('http://localhost:3000/comments', {
    method: 'POST',
    body:JSON.stringify(newTeacher),
    headers: {
      'Content-Type': 'application/json',
    },

  });
   if(response.ok) {
     const responseData = await response.text();
     console.log(responseData);
     alert("Teacher was added!");
   }
   else alert("ERROR")
  document.getElementById("add-teacher-popup").style.display = "none";

});

//DRAW A TABLE
for (const editArrKey of document.getElementsByTagName("th")) {
  editArrKey.addEventListener("click", function (event){
    let top = editArrKey.firstElementChild;
    if(top.className === "arrow-bottom"){
      currentArray =[...arraySort(true, editArrKey.textContent)];
      drawTable(currentPage,  currentArray);
      editArrKey.firstElementChild.className += " arrow-top";
    }
    else{
      currentArray =[...arraySort(false, editArrKey.textContent)];
      drawTable(currentPage, currentArray);
      top.className ="arrow-bottom" ;
    }
  });
}
// count pages for table
function pageInTable() {
  let countPages = Math.ceil(teachers.length / 10);
  const containerButton = document.getElementById("buttons");
  let html = "";
  for (let i = 1; i <= countPages; i++) {
    if (i === 1) html += `<button class = "table-buttons active">${i}</button>`
    else html += `<button class = "table-buttons notActive">${i}</button>`
  }
  html += `<button class="dots" id="dots">...</button>
           <button class="last notActive" id="last">Last</button>`
  containerButton.innerHTML = html;
  for (const button of document.getElementsByClassName("table-buttons")) {
    button.addEventListener("click", function () {
      changeButtonColor();
      currentPage =button.textContent;
      button.className = "table-buttons active";
      drawTable(button.textContent, currentArray);

    });
  }
  document.getElementById("last").addEventListener("click",function () {
    changeButtonColor();
    document.getElementById("last").className = "last active";
    drawTable(countPages, currentArray);
  })
  document.getElementById("dots").addEventListener("click", function (){
    changeButtonColor();
    document.getElementById("dots").className = "dots active";

    currentPage =teachers.length;
    drawTable(currentPage, currentArray);
  })
}
function changeButtonColor() {
  document.getElementById("last").className = "last notActive";
  document.getElementById("dots").className = "dots notActive";
  for (const button of document.getElementsByClassName("table-buttons")) {
    if (button.textContent === currentPage) {
      button.className = "table-buttons notActive";
    }
  }
}
function drawTable(n, inputArray) {
  let table = document.getElementById("tbody");
  let html = '';
  let indexFirst = (n*10) - 10;
  let indexLast = n*10;
  if(n != teachers.length) inputArray = inputArray.slice(indexFirst, indexLast);
  for (let i =0; i < inputArray.length; i++) {
    html +=
        `<tr>
         <td class="name-in-table">${inputArray[i].full_name}</td>
         <td>${inputArray[i].course}</td>
         <td>${inputArray[i].age}</td>
         <td>${inputArray[i].gender}</td>
         <td>${inputArray[i].country}</td>
         </tr>`
  }
  table.innerHTML = html;
}
// FAVORITE TEACHERS SECTION
//render favorites teacher
function renderFavorites() {
  let carousel = document.getElementById("slides");
  let html = "";
  for (let i = 0; i < favoritesTeachers.length; i++) {
    html += `
    <div class = "slide">`

    if (favoritesTeachers[i].picture_large === undefined) {
      html
          += `  <div class="cycle-photo without-photo" >
                  <span class="styled-image">${favoritesTeachers[i].first_name.charAt(0)}.${favoritesTeachers[i].last_name.charAt(0)}</span>
                  </div>`
    } else {
      html
          += ` <div class="cycle-photo" >
                 <img class ="imgClass" src=${favoritesTeachers[i].picture_large} alt="Photo is absent"/>
                 </div>`
    }
    html +=`
    <p class = "name-teacher">${favoritesTeachers[i].first_name}</p>
    <p class = "name-teacher">${favoritesTeachers[i].last_name}</p>
    <p class = "country">${favoritesTeachers[i].country}</p>
    </div>`
  }
  carousel.innerHTML = html;
  if(favoritesTeachers.length < 5){
    carousel.style.justifyContent ="center";  }
  else {
    carousel.style.justifyContent ="";
  }
}
let position = 0;
let width;
let count =5;
let list = document.getElementById('slider-track');
let carousel = document.getElementById("slides");
document.getElementById("button-left").addEventListener("click",carouselLeft);
function carouselLeft() {
  if (favoritesTeachers.length > count) {
    width = list.offsetWidth / count;
    position += width;
    position = Math.min(position, 0)
    carousel.style.transform = `translate(${position}px)`
  }
}
let buttonRight = document.getElementById("button-right");
buttonRight.addEventListener("click",carouseRight);
function carouseRight(){
  if(favoritesTeachers.length >count){
    width =list.offsetWidth / count;
    position -= width;
    position = Math.max(position, -width * (favoritesTeachers.length - count));
    carousel.style.transform = `translate(${position}px)`;
  }
}
//ANOTHER METHODS FROM 2 LAB
// ex 1
export function createArrayTeachers() {
  const array = [];
  for (const arrayElement of teachers) {
   // const name = `${arrayElement.name.first} ${arrayElement.name.last}`;
    const courses = ['Mathematics', 'Physics', 'English', 'Computer Science', 'Dancing', 'Chess', 'Biology', 'Chemistry', 'Law', 'Art', 'Medicine', 'Statistics'];
    const bg_color = ['#F65F44', '#2c787a', 'rebeccapurple', 'darksalmon', 'cornsilk', 'red'];
    const newObject = {
      gender: arrayElement.gender,
      title: arrayElement.name.title,
      full_name: `${arrayElement.name.first} ${arrayElement.name.last}`,
      first_name: arrayElement.name.first,
      last_name: arrayElement.name.last,
      city: arrayElement.location.city,
      state: arrayElement.location.state,
      country: arrayElement.location.country,
      postcode: arrayElement.location.postcode,
      coordinates: arrayElement.location.coordinates.longitude,
      timezone: arrayElement.location.timezone.description,
      email: arrayElement.email,
      b_date: arrayElement.dob.date,
      age: arrayElement.dob.age,
      phone: arrayElement.cell,
      picture_large: arrayElement.picture.large,
      picture_thumbnail: arrayElement.picture.thumbnail,
      id: (arrayElement.id.value !== null ? arrayElement.id.value : randomId()),
      favorite: false,
      course: courses[Math.floor(Math.random() * courses.length)],
      bg_color: bg_color[Math.floor(Math.random() * bg_color.length)],
      note: 'Lorem Ipsum dolor sit amet, consectetur adipisicing elit. A atque facere, hic impedit itaque molestiae sit tempore? Ea in mollitia natus rerum voluptates.',
    };
    array.push(newObject);
  }
  currentArray =[...array];
  teachers =[...array];
  favoritesTeachers = teachers.filter( teachers => teachers.favorite);
  }
export function editArrays() {
  const array = [];
  for (const arrayElement of randomUserMock) {
    const name = `${arrayElement.name.first} ${arrayElement.name.last}`;
    const courses = ['Mathematics', 'Physics', 'English', 'Computer Science', 'Dancing', 'Chess', 'Biology', 'Chemistry', 'Law', 'Art', 'Medicine', 'Statistics'];
    const bg_color = ['#F65F44', '#2c787a', 'rebeccapurple', 'darksalmon', 'cornsilk', 'red'];
    const newObject = {
      gender: arrayElement.gender,
      title: arrayElement.name.title,
      full_name: `${arrayElement.name.first} ${arrayElement.name.last}`,
      first_name: arrayElement.name.first,
      last_name: arrayElement.name.last,
      city: arrayElement.location.city,
      state: arrayElement.location.state,
      country: arrayElement.location.country,
      postcode: arrayElement.location.postcode,
      coordinates: arrayElement.location.coordinates,
      timezone: arrayElement.location.timezone,
      email: arrayElement.email,
      b_date: arrayElement.dob.date,
      age: arrayElement.dob.age,
      phone: arrayElement.phone,
      picture_large: arrayElement.picture.large,
      picture_thumbnail: arrayElement.picture.thumbnail,
      id: (arrayElement.id.value !== null ? arrayElement.id.value : randomId()),
      favorite: true,
      course: courses[Math.floor(Math.random() * courses.length)],
      bg_color: bg_color[Math.floor(Math.random() * bg_color.length)],
      note: 'Lorem Ipsum dolor sit amet, consectetur adipisicing elit. A atque facere, hic impedit itaque molestiae sit tempore? Ea in mollitia natus rerum voluptates.',
    };
    array.push(newObject);
  }
  for (const arrayElement of additionalUsers) {
    const name = arrayElement.full_name.split(" ");
    const courses = ['Mathematics', 'Physics', 'English', 'Computer Science', 'Dancing', 'Chess', 'Biology', 'Chemistry', 'Law', 'Art', 'Medicine', 'Statistics'];
    const bg_color = ['#F65F44', '#2c787a', 'rebeccapurple', 'darksalmon', 'cornsilk', 'red'];
    arrayElement.first_name =name[0];
    arrayElement.last_name = name[1];
    if(arrayElement.course===null) arrayElement.course = courses[Math.floor(Math.random() * courses.length)];
    if(arrayElement.bg_color === null) arrayElement.bg_color = bg_color[Math.floor(Math.random() * bg_color.length)];
    if(arrayElement.id === null) arrayElement.id = randomId();
  }

  editArr = [
    ...array,
    ...additionalUsers.filter(
        (teacher) => !array.some((exist) => exist.full_name === teacher.full_name),
    ),
  ];
  favoritesTeachers = editArr.filter((teacher) => teacher.favorite);

  //currentArray =[...editArr];
  return editArr;
}
// ex 2
function validateObject(object) {
  if (typeof object.full_name === 'string'
      && typeof object.gender === 'string'
      && typeof object.note === 'string'
      && typeof object.state === 'string'
      && typeof object.city === 'string'
      && typeof object.country === 'string'
      && object.full_name.charAt(0) === object.full_name.charAt(0).toUpperCase()
      && object.note.charAt(0) === object.note.charAt(0).toUpperCase()
      && object.state.charAt(0) === object.state.charAt(0).toUpperCase()
      && /^[A-Z]/.test(object.city)
      && /^[A-Z]/.test(object.country)
      && /^[0-9]/.test(object.phone)
      && typeof object.age === 'number'
      && object.email.includes('@')) { return true; }
  return false;
}
function toValidateObj(object) {
  if (typeof object.full_name !== 'string') { object.full_name = String(object.full_name); }
  if (typeof object.gender !== 'string') { object.gender = String(object.gender); }
  if (typeof object.note !== 'string') { object.note = String(object.note); }
  if(object.state!==null){
    if (typeof object.state !== 'string') { object.state = String(object.state); }
    if (!/^[A-Z]/.test(object.state)) { object.state = object.state.charAt(0).toUpperCase() + object.state.slice(1); }

  }
  if (typeof object.city !== 'string') { object.city = String(object.city); }
  if (typeof object.country !== 'string') { object.country = String(object.country); }
  if (object.full_name.charAt(0) !== object.full_name.charAt(0).toUpperCase()) {
    let name = object.full_name.split(" ");
    name[0] = name[0].charAt(0).toUpperCase() + name[0].slice(1);
    name[1] = name[1].charAt(0).toUpperCase() + name[1].slice(1);
    object.full_name = name[0] +" "+name[1];
  }
  if(object.first_name.charAt(0) !== object.first_name.charAt(0).toUpperCase()) {object.first_name = object.first_name.charAt(0).toUpperCase() +  object.first_name.slice(1);}
  if(object.last_name.charAt(0) !== object.last_name.charAt(0).toUpperCase()) {object.last_name = object.last_name.charAt(0).toUpperCase() +  object.last_name.slice(1);}
  if (!/^[A-Z]/.test(object.city)) { object.city = object.city.charAt(0).toUpperCase() + object.city.slice(1); }
  if (!/^[A-Z]/.test(object.country)) { object.country = object.country.charAt(0).toUpperCase() + object.country.slice(1); }
  if (object.note.charAt(0) !== object.note.charAt(0).toUpperCase()) { object.note = object.note.charAt(0).toUpperCase() + object.note.slice(1); }
  return object;
}
// ex 3
function filtration(filterValue) {
  const filteredArr = [];
  const num = filterValue.age.split("-");
  const min = parseInt(num[0]);
  const max = parseInt(num[1]);

  for (const object of Object.values(teachers)) {
    if ((object.picture_large !== undefined && filterValue.photo) || (object.picture_large === undefined && !filterValue.photo)) {
      if (object.age >= min && object.age <= max) {
        if (object.country === filterValue.country && object.gender === filterValue.gender && object.favorite === filterValue.favorite) {
          filteredArr.push(object);
        }
      }
    }
  }

  return filteredArr;
}
// ex 4
// true up / false down
function arraySort(bool, parameter) {
  const copy = [...teachers];
  if(parameter === "Age") parameter = "age";
  if (parameter === 'age') copy.sort((a, b) => a[parameter] - b[parameter]);
  else {
    if(parameter === "Name") parameter = "full_name";
    if(parameter === "Speciality") parameter = "course";
    if(parameter === "Gender") parameter = "gender";
    if(parameter === "Nationality") parameter ="country";
    copy.sort((a, b) => {
      return a[parameter].toLowerCase().localeCompare(b[parameter].toLowerCase());
    });
  } if (!bool) return copy.reverse();
  return copy;
}
// ex 5
function search(input) {
  const foundElement = teachers.find((value) => {

    return (
        value.full_name.toLowerCase().includes(input.toLowerCase())
        || (value.note && value.note.toLowerCase().includes(input.toLowerCase()))
        || value.age === parseInt(input)
    );
  });
  return foundElement || false;
}

// ex 6
function percent(parameter, value) {
  let result = [];
  if (parameter === 'age') {
    result = editArr.filter((teachers) => teachers.age >= value);
  } else result = editArr.filter((teachers) => teachers[parameter] === value);
  return (result.length * 100) / editArr.length;
}


/**
 function tester() {
  editArrays();

  console.log("--------Task 1-------- ");
  console.log("To edit randomArray");
  console.log(Object.entries(editArrays()));
}
 tester();
 //editArrays();

 console.log("--------Task 2-------- ");
 console.log("Test if input object is validate : ");
 console.log(validateObject(editArr[0]));

 if(!validateObject(editArr[0])) {
         console.log("If it false will validate object : ");
         console.log(toValidateObj(editArr[0]));
         console.log("Test if input object is validate : ");
         console.log(validateObject(editArr[0]));
     }

 // ex 3
 let filter ={
            country: "Germany",
            age: "30-70",
            gender: "female",
            favorite: false,
        }
 console.log("--------Task 3-------- " + "\n Filtration by:");
 console.log(filter)
 console.log(filtration(filter));

 //ex 4
 console.log("Search by note : cats");
 console.log(search("cats"))
 // ex 6
 console.log("--------Task 4-------- " + "\n Sorting by increase. Parameter : age");
 console.log(arraySort(true, "age"));
 console.log("Sorting by decrease. Parameter : country");
 console.log(arraySort(false, "country"));

 //ex 5
 console.log("--------Task 5-------- " + "\n Search by Name : Adeline Weigand")
 console.log(search("Adeline Weigand"));
 console.log("Search by age : 65");
 console.log(search("65"))
 console.log(percent("country", 'France'))
 console.log(percent("age", '27'))

 }
 //tester();
 * */
